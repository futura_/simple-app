FROM node:fermium-slim

COPY package* .

RUN npm install --production

COPY . .

CMD scripts/entrypoint.sh
