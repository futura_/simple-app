#!/usr/bin/env bash
set -eo pipefail

# check if all envs are populated
for var_name in $(cut -d = -f 1 .env.example | tr '\n' ' '); do
    [ -z "${!var_name}" ] && echo "$var_name is unset." && exit 1
done

connstr="postgres://${username}:${password}@${host}:${port}/${database}"
PATH=$PWD/node_modules/.bin:$PATH

sequelize-cli db:migrate --url "$connstr"
sequelize-cli db:seed:all --url "$connstr"

export NODE_ENV=${NODE_ENV:-production}

exec node ./bin/www
